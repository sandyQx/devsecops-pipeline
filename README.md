# DevSecOps Infrastrukture Pipeline 
This repo is contain of Jenkins, SonarQube, Sonatype Nexus OSS and also Posgress database, all of that tools will help you to manage aplication and implement Secure CI/CD Automation.

you can more explore function of that tools.

Jenkins => http://localhost:8080

SonarQube => http://localhost:9000

Nexus => http://localhost:8081


### Installation
1. clone repository 
```bash
git clone https://gitlab.com/sandyQx/devsecops-pipeline.git
```

2. make folder data witeble
```bash
cd devsecops-pipeline
sudo chmod -R 777 data
```

3. running yml script 
```bash
docker-compose up -d
```
for jenkins part you will need more step to complete installation part.

After you runing all of container so to complete setup jenkins installation you need signin in to jenkins container and then grab the devault password from jenkins environment
open your terminal and type 
```bash
docker exec -uroot -it {container id} bash
cat /var/lib/jenkins/secrets/initialAdminPassword
```
after you get the password then you can put that password on the jenkins installation(when you run jenkins at the firstime jenkins will ask to put initialAdminPassword )


